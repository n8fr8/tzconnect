---
layout: post
title:  "Keep Signal Safe"
date:   2020-10-21 10:10:00 -0300
categories: digisec 
---

Using [Signal](https://signal.org)? Well, make sure to use it safely, and keep your community connected.

With this guide, you will learn how to:

* Lock Down Your Phone
* Hide Messages on Your Lock Screen
* Use Disappearing Messages
* Verify Your Contacts


[English Guide](/assets/TZCONNECT2020KeepSignalSafe.pdf)
