---
layout: post
title:  "Safe Sisters Guide"
date:   2020-10-20 10:10:00 -0300
categories: digisec 
---

From [SafeSisters.net](https://safesisters.net)

"We wrote this booklet to help our sisters learn about problems that we might run into on the internet (like leaked or stolen photos, viruses, and scams), how we can make informed decisions every day to protect ourselves, and to make the internet a safe space for ourselves, our families, and all women. Our mission is make digital security less complicated and more relevant to real users and to encourage all women and girls to take online safety into their own hands.  We hope this booklet will help readers see that the most effective ways to protect yourself online are common sense strategies we already use offline every day."

![](/assets/safesisters.jpg)

[Kiswahili Guide](https://safesisters.net/wp-content/uploads/2019/10/Safe-Sisters-Kiswahili.pdf)

[English Guide](https://safesisters.net/wp-content/uploads/2019/04/Safe-Sister-Guide-revised-1.pdf)
